# Example of creating a release from Gitlab CD/CI build artifacts

In this `.gitlab-ci.yml` example, it's possible to create a release from the files that were created by the build stage.

Source: [https://stackoverflow.com/questions/60212032/attach-pipeline-artifact-to-release-in-gitlab](https://stackoverflow.com/questions/60212032/attach-pipeline-artifact-to-release-in-gitlab)
